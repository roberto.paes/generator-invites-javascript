var today = new Date();
var expiry = new Date(today.getTime() + 30 * 24 * 3600 * 1000); // plus 30 days

function setCookieParam(name,param){

    document.cookie = name + "=" + param + "; path=/; expires=" + expiry.toGMTString();

}
function getCookieValue(json,name){
    if(json == null){
        return {value:''}
    }
    const resposta = json.find((item,index) => {
       return item.name === name
      });
      return resposta
}
// function setCookieInput(name, id) {
//     var element = document.getElementById(id);
//    var type =  element.getAttribute('type');
//    var elementValue = ''
//    if(type == 'textarea'){
    
//      elementValue = CKEDITOR.instances[id].getData(); 
//    }else{
//        elementValue = element.value; 
//    }
 
 
//  var lastJson = JSON.parse(getCookie('form'))
// if(lastJson){
//  const foundIndex = lastJson.findIndex(function (item) {
//  return item.name === name;
//  });
//  lastJson[foundIndex] = {name:name,value : elementValue}
// }else{
//     lastJson = getallFormtoJson('myForm')
// }
//     document.cookie = 'form' + "=" + JSON.stringify(lastJson) + "; path=/; expires=" + expiry.toGMTString();
    
// }


function getallFormtoJson(element){
    var form = document.getElementById(element);
    var json = [];
    for(var i = 0; i < form.length; i++) {
        var name = form[i].getAttribute('name');
        var id = form[i].getAttribute('id');
        var type = form[i].getAttribute('type');
        var getForm = form[i].getAttribute('toForm');

        var value = ''
       
        if(type != 'button' && getForm == undefined && form[i].tagName != 'DIV'){ 
      
        if(type == 'textarea'){
   
            value =  escape(CKEDITOR.instances[name].getData())
          }else{
            value = form[i].value
          }
                json.push({name:name,value:value})
        }
        
    
  
    } 
   
    return json

}
function setCookieForm(element) {
    if(isCookieEnabled){
    var form = document.getElementById(element);
    for(var i = 0; i < form.length; i++) {
        var name = form[i].getAttribute('name');
        var id = form[i].getAttribute('id');
        var type = form[i].getAttribute('type');
        
 var value = form[i].value;
if(value != undefined && type != 'button'){
  
    setCookieInput(name, id);


}
    }
}
}
function FillForm(element,session) {
 
   
    var form = document.getElementById(element);
    for(var i = 0; i < form.length; i++) {
        var name = form[i].getAttribute('name');
        var id = form[i].getAttribute('id');
        var type = form[i].getAttribute('type');
        var getForm = form[i].getAttribute('toForm');
 var value = form[i].value;
if(type != 'button' && type != 'hidden' && getForm == undefined && form[i].tagName != 'DIV' ){ 
    
    if(type == 'textarea'){
  
        if(JSON.parse(session)[name] != undefined){
            CKEDITOR.instances[id].setData(unescape(JSON.parse(session)[name]))

        }
      }else{
        if(JSON.parse(session)[name] != undefined){
  form[i].value = JSON.parse(session)[name]
        }
      }
      

  

}
    }

}


function getCookie(name) {
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
  
    return (value != null) ? unescape(value[1]) : null;
}
$(document).ready(function(){   
   
      if(!isCookieEnabled()){
    setTimeout(function () {
        $("#cookieConsent").fadeIn(300);
     }, 500);
    $("#closeCookieConsent, .cookieConsentOK").click(function() {
        setCookieParam('accept','1')
        $("#cookieConsent").fadeOut(300);
    }); 
      }
}); 

function isCookieEnabled(){
    var element =  getCookie('accept')
    
    if(element == '1'){ 
         console.log('retornando true')
        return true
      

    }else if(element == null){
        console.log('vindo nulo')
        return false
    }
 }

function deleteAllCookies(name){
    const pageAccessedByReload = (
        (window.performance.navigation && window.performance.navigation.type === 1) ||
          window.performance
            .getEntriesByType('navigation')
            .map((nav) => nav.type)
            .includes('reload')
      );
if(pageAccessedByReload){
 document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
console.log('cleaning.. '+ name)
}

    
  //      console.info( "This page is not reloaded");
      

}