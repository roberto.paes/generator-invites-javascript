CKEDITOR.plugins.add( 'simpleImageUpload', {
    init: function( editor ) {
        var fileDialog = $('<input type="file">');
        
        fileDialog.on('change', function (e) {
            var uploadUrl = '/imgupload';
            var file = fileDialog[0].files[0];
            
            if (file.name.match(/\.(png|jpeg)$/)) {
                var reader = new FileReader();
                reader.onload = function() { 
             var teste = URL.createObjectURL(file)
        
             
                convertFileToBase64viaFileReader(teste, function(base64Img){
                     
                  
                        var ele = editor.document.createElement('img');
                        ele.setAttribute('src', 'http://127.0.0.1:5050/imgupload/' + base64Img.split(';base64,').pop());
                        editor.insertElement(ele);
                 
                    
              });
            }
            reader.readAsDataURL(file);    
        
        }
               
	

        })
        editor.ui.addButton( 'Image', {
            label: 'Insert Image',
            command: 'openDialog',
            toolbar: 'insert'
        });
        editor.addCommand('openDialog', {
            exec: function(editor) {
                fileDialog.click();
            }
        })
    }
});