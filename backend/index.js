var express = require('express')
var bodyParser = require('body-parser')
const path = require('path');
var pdfController = require('./Controllers/pdf-controller')
var formController = require('./Controllers/form-controller')
var emailController = require('./Controllers/email-controller')
var session = require('express-session')
var inviteController = require('./Controllers/invites-controller')
var serverModel = require('./Model/server-model')
var testModels = require('./Model/Tests/TestRoutes/TestRoutes')
var formValidateModel = require('./Model/form-validade-model')
var app = express()
const fs = require('fs').promises;
const MimeNode = require('nodemailer/lib/mime-node');
var urlencodedParser = bodyParser.urlencoded({})
app.set('view engine','ejs')
app.set('views', path.join(__dirname, './views'));
app.use(bodyParser({limit:'50mb'}))
app.use(session({
name: 'session',
secret: 'secret-key',
resave: false
}))
app.use('/public', express.static(path.join(__dirname, './public')));
testModels.RegisterRouteModels(app,urlencodedParser)
app.get('/', (req,res) => {
  return inviteController.editInvites(req,res)
});
app.post('/', urlencodedParser, function (req,res)  {


var json = JSON.parse(unescape(req.body.json))

  const getById = (name) => {
    const resposta = json.form.find((item) => {
      return item.name === name
    });
    return resposta
  }
req.session.view = req.params.view 
if(getById('convidadosLista').value){
    req.session.convidadosLista = JSON.parse(getById('convidadosLista').value)
}
  req.session.txtMessage = getById('txtMessage').value
  req.session.linha = getById('linha').value
  req.session.largura = getById('largura').value
  req.session.altura = getById('altura').value
  req.session.invitetype = getById('invitetype').value
  req.session.convites = getById('convites').value
  req.session.imagebackground = getById('imagebackground').value
  if(getById('mail_subject')){
req.session.mail_subject = getById('mail_subject').value
  }
  
  return inviteController.editInvites(req,res,json)
});
app.post('/renderpdfpage', urlencodedParser,function (req,res) {
return pdfController.PdfPage(req,res,req.body)
});
app.post('/prerender', urlencodedParser, async function (req, res) {
  
var body = {  
  txtMessage: req.body.txtMessage,
  linha: req.body.linha,
  largura: req.body.largura,
  altura: req.body.altura,
  invitetype: req.body.invitetype,
  convites: req.body.convites
}
//await formValidateModel.formValidate(body)
  req.session.convidadosLista = req.body.convidadosLista
  req.session.txtMessage = req.body.txtMessage
  req.session.linha = req.body.linha
  req.session.largura = req.body.largura
  req.session.altura = req.body.altura
  req.session.invitetype = req.body.invitetype
  req.session.convites = req.body.convites
  req.session.imagebackground = req.body.imagebackground
  req.session.mail_subject = req.body.mail_subject
return formController.formSender(req,res)


});
app.post('/sendmail', urlencodedParser, async function (req,res){
  try {  
    var body = {  
      txtMessage: req.body.txtMessage,
      convidadosLista:JSON.parse(req.body.convidadosLista),
      mail_subject: req.body.mail_subject
    }
  await formValidateModel.emailValidate(body)
  emailController.sendMail(req,res)
 res.sendStatus(200);
  }catch(err){
   console.log('erro ao enviar email')
 res.json(err);
  }
 });
app.get('/emailgenerator/:username&info=:html', urlencodedParser,function (req,res){

  return emailController.viewGenerator(req,res)
  });
app.get('/emailgenerator/none', urlencodedParser,function (req,res){

    return emailController.viewInformation(req,res)
});
app.get('/pdfgenerator/:id', urlencodedParser,function (req,res){
return pdfController.PdfGenerator(req,res,req.params.id)
});
app.listen(serverModel.Port,serverModel.Host)
console.log('server started at ' + serverModel.Port + serverModel.Host )
