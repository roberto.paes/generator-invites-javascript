var formModel = require('./../Model/form-inviteTypes-model')
var multiFormModel = require('./../Model/form-multiform-model')


module.exports = {
    editInvites : function(req,res,json){
 
        const resultadoOptions = formModel.getAllOptions()
        const OptionsItensViewModel = resultadoOptions.map((item) => {
          return {
            id: item.id,
            descricao: item.descricao
          }
        })
        const MultiFormOptions = multiFormModel.getAllWindow()
        const MultiFormViewModel = MultiFormOptions.map((item) => {
          return {
            id: item.id,
            descricao: item.descricao,
            placeholder: item.placeholder,
            view: item.view,
            inflate:item.inflate,
            customscript: item.customscript,
            style: item.style
          }
        })
      
      

  const getViewModel = {
    Options: OptionsItensViewModel,
    OptionsById: formModel.getOptionsPorId,
    MultiFormOptions: MultiFormViewModel,
    MultiFormByiD: multiFormModel.getWindowById
  }
  var id = 0
  var view = false
  if(json != undefined){
     id =  json.id 
     view = json.view
  }
  res.render('principal',{
    invitetype : req.session.invitetype,
    session: JSON.stringify(req.session),
    id: id,
    view : view,
    Options : getViewModel

  
    
});
        
    },
    }