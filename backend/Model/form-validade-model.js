const joi = require('joi'); 
  const formSchema = joi.object({
    txtMessage: joi.string(),
    linha: joi.number().positive(),
    largura: joi.number().positive(),
    altura: joi.number().positive(),
    invitetype: joi.number().positive(),
    convites: joi.number().positive(),
}).error((errors) => {
    errors.map((err) => {
         throw (err.path)
 })
})
const emailSchema = joi.object({
    txtMessage: joi.string(),
    convidadosLista: joi.array().items(joi.object({
        convidado : joi.string(),
        email: joi.string().email(),
        confirmado: joi.bool()
    })),
    mail_subject: joi.string()
}).error((errors) => {
errors.map((err) => {
    throw (err.path)
})
});
async function formValidator(body){
    await formSchema.validateAsync(body);
}
async function emailValidator(body){
    await emailSchema.validateAsync(body);
}
module.exports = {
   formValidate : formValidator,
   emailValidate : emailValidator,
}