const data = [
    {
      id: 0,
      descricao: 'Por lista de convidados'
    }, 
    {
      id: 1,
      descricao: 'Convite único'
    },
    {
      id: 2,
      descricao: 'Por email'
    }
   
  
  ]
  
  const getById = (id) => {
  
    const resposta = data.find((item) => {
      return item.id === id
    });
  
    return resposta
  
  }
  
  const getAll = () => {
    return data;
  }
  
  module.exports = {
    getAllOptions: getAll,
    getOptionsPorId: getById
  }
  