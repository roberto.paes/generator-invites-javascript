

const customFormScript = "nextPageInflator(Inflator00) FillInflatorMenu()"  //"function(){ FillInflatorMenu() nextPageInflator(Inflator00) }"
const Multiview = [
  {view: './../MultiForm/InviteType.ejs', name:'Tipo de convite'},
  {view: './../MultiForm/InviteUser.ejs', name:'Configurar lista'},
  {view: './../MultiForm/FormConfig.ejs', name:'Configurar convite'},
  {view: './../MultiForm/InviteEditor.ejs', name:'Editar convite'}

]
const data = [
    
    {
      id: 0,
      placeholder: 'Início',
      descricao: 'Início',
      view: ['home.ejs'],
      style: 'width:200px;height:40px;'
    },
    {
      id: 1,
      placeholder: 'Convites',
      descricao: 'Escolha seu tipo de convite',
      view: Multiview,
      inflate: './components/formComponent',
      style: 'width:200px;height:40px;'
    },
    {
      id: 2,
      placeholder: 'Sobre',
      descricao: 'Sobre',
      view: ['about.ejs'],
      style: 'width:200px;height:40px;'
    }
   
  
  ]
  
  const getById = (id) => {
  
    const resposta = data.find((item) => {
      return item.id === id
    });
  
    return resposta
  
  }
  
  const getAll = () => {
    return data;
  }
  
  module.exports = {
    getAllWindow: getAll,
    getWindowById: getById
  }
  