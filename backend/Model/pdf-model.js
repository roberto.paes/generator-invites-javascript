const fs = require('fs').promises;
var signature = require( "cookie-signature" )
var serverModel = require('./server-model')
const puppeteer = require('puppeteer')

function handlerType(pdf,res,option){
   if(option == 1){
   res.attachment('convidados.pdf');

        res.end(pdf)
        }else{
          res.send(pdf)
        }
}
exports.PdfGenerator = async (res,req,option) =>{
 
  let browserConfig = {}
  


      if(serverModel.Docker){
         browserConfig = {
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
          executablePath: '/usr/bin/chromium-browser'
      };
  }else{
    browserConfig = {
      headless: true
  };

  }

 

  var browser = await puppeteer.launch(browserConfig)
  const page = await browser.newPage();
 
 await page.setRequestInterception(true);
 page.once('request',  interceptedRequest => {

      var json = {altura:req.session.altura,largura:req.session.largura,convidadosLista:req.session.convidadosLista,linha: req.session.linha,invitetype: req.session.invitetype,convites: req.session.convites,txtMessage: req.session.txtMessage}

      var data = {
          method: 'POST',
          postData: JSON.stringify({img:req.session.imagebackground,json:json}),
          headers: {"content-type": "application/json"}

      };
      
      // Request modified... finish sending! 
      interceptedRequest.continue(data);
     
      page.setRequestInterception(false);
  
  }) 

 await page.goto('http://'+serverModel.Host +':' +serverModel.Port +'/renderpdfpage', { waitUntil: "networkidle0" });

  
           var pdf = await page.pdf({
              format: 'A4'
            });

   await page.close()   
          await browser.close()   
       
           res.setHeader('Content-type', 'application/pdf')

      handlerType(pdf,res,option)
     
      
    };